  Today august 21 and the entire month i was using the docs in [Django 2.0](https://docs.djangoproject.com/es/2.1/)  for resolve a little bit complex  Querysets, or an a ascii problem with the usernames.
When im reading the stack overflow answer, usually  tried to search  the docs of the library what the community are using to resolve, cause I don't like the copy past the other work and don't understand what they really are doing with the snipped or the answer .

Now im using Osx High Sierra, i dont really think my "main" programming language is Python 3.x
but i've been coding for a year and five months, in the institute we learn Java,Php and C#. 
Also im using Angular 6 in other project.

####### Thanks for the opportunity. I dont have much time to resolve all the points of the exercise
####### Cause im working as fulltime and studing during the nights 
####### There is a little snipper 

class UserForm(forms.ModelForm):
	first_name = forms.CharField(required=True)
	last_name = forms.CharField(required=True)
	email = forms.EmailField(required=True)
	confirm_email = forms.EmailField(required=True)


	class Meta:
		model = User
		fields = ("first_name",)

	def clean(self):
		required_message_error = "Este campo es requerido."
		email_in_use_error = "Este correo esta en uso."
		email = self.cleaned_data.get("email")
		confirm_email = self.cleaned_data.get("confirm_email")
		if not email:
			self.add_error("email", required_message_error)

		if not confirm_email:
			self.add_error("confirm_email", required_message_error)
		if User.objects.filter(email=email).exists():
			self.add_error("email", email_in_use_error)
		if email != confirm_email:
			self.add_error("email", "Las direcciones de correo no coinciden.")


class AccountCreationForm(UserForm):
	password1 = forms.CharField(required=True)
	password2 = forms.CharField(required=True)
	organization = forms.CharField(required=False)
	terms_acceptance = forms.BooleanField(required=True)
	captcha = ReCaptchaField(attrs={"lang": "es"})

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.fields["is_active"].initial = False

	class Meta(UserForm.Meta):
		fields = UserForm.Meta.fields + ("is_active",)
